Para usar os scripts de forma nativa, siga o seguinte passo a passo:

1° Vá para seu HOME:
    -   cd

2° Abra o seu arquivo de configuração de terminal
    -   nano .bashrc

3° De permissão de execusão ao .sh
    -   sudo chmod +x <arquivo.sh>

4° Crie um alias para seu script
    alias <nome_que_quiser> = 'sh <arquivo.sh>'

5º Encerre sua sessão e teste seu comando no terminal

Como deve funcionar:
![alt text](image.png)
![alt text](image-1.png)
![alt text](image-2.png)